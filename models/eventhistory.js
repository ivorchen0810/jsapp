var mongoose = require("mongoose"),
    Schema = mongoose.Schema;

var EventsSchema = mongoose.Schema({
    owner: { type: Schema.Types.ObjectId, ref:'User',required: true },
    description: { type: String, required: true },
    createdAt: { type: Date, default: Date.now },
    location:{type:String, index:true,require:true}
});

var Events = mongoose.model("Events", EventsSchema);
module.exports = Events;