var express = require("express");
var User = require("../models/user");
var EventHistory=require("../models/eventhistory");
var router = express.Router();
var passport = require("passport");
router.use(function(req, res, next) {
    res.locals.currentUser = req.user;
    res.locals.errors = req.flash("error");
    res.locals.infos = req.flash("info");
    next();
});

router.get("/signup", function(req, res) {
    res.render("signup");
});
router.post("/signup", function(req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    User.findOne({ username: username }, function(err, user) {
        if (err) { return next(err); }
        if (user) {
            req.flash("error", "User already exists");
            return res.redirect("/signup");
        }
        var newUser = new User({
            username: username,
            password: password
        });
        newUser.save(next);
    });
}, passport.authenticate("login", {
    successRedirect: "/",
    failureRedirect: "/signup",
    failureFlash: true
}));

router.get("/users/:username", function(req, res, next) {
    User.findOne({ username: req.params.username }, function(err, user) {
        if (err) { return next(err); }
        if (!user) { return next(404); }
        EventHistory.find({owner:user._id},function (err,eventhistory) {
            if(err) throw err;
            if(!eventhistory){
                console.log("No event");
                res.render("profile", { user: user ,eventhisotry:null}); }
            else {
                console.log(eventhistory);
                res.render("profile", {
                    user: user,
                    eventhistory: eventhistory});
            }
        });

    });
});

router.get("/login", function(req, res) {
    res.render("login");
});

router.post("/login", passport.authenticate("login", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true
}));

router.get("/logout", function(req, res) {
    req.logout();
    res.redirect("/");
});

router.get("/", function(req, res, next) {
    User.find()
        .sort({ createdAt: "descending" })
        .exec(function(err, users) {
            if (err) { return next(err); }
            res.render("index", { users: users });
        });
});
function ensureAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        req.flash("info", "You must be logged in to see this page.");
        res.redirect("/login");
    }
}
router.get("/edit", ensureAuthenticated, function(req, res) {
    res.render("edit");
});

router.post("/edit", ensureAuthenticated, function(req, res, next) {
    req.user.displayName = req.body.displayname;
    req.user.bio = req.body.bio;
    req.user.save(function(err) {
        if (err) {
            next(err);
            return;
        }
        req.flash("info", "Profile updated!");
        res.redirect("/edit");
    });
});

router.get("/addevents", ensureAuthenticated, function(req, res) {
    res.render("addevents");
});
router.post("/addevents", ensureAuthenticated, function(req, res, next) {
    var newEvent=new EventHistory({
        owner: req.user._id,
        description: req.body.description,
        location:req.body.location
    });
    newEvent.save();
    console.log("Finish saving"+req.body.description+" "+req.body.location);
    //res.flash("Event Saved");
    res.redirect("/users/"+req.user.username);
    // EventHistory.findOne({owner:this.currentUser._id},function (err, EventHistory) {
    //     if(err) throw err;
    //     render("profile",{_event:EventHistory});
    // })
});

module.exports = router;